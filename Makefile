# Copyright (c) 2023 mira

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Installation prefix, update it to fit with your system
PREFIX = /usr/local

PROGNAME = capitalize

CXXFLAGS = -D_POSIX_C_SOURCE=200809L
CFLAGS = -Wall -pedantic -pipe -O2 ${CXXFLAGS}

CC = cc
SRC = capitalize.c
OBJ = ${SRC:.c=.o}

all: flags ${PROGNAME}

flags:
	@echo "${PROGNAME} build options:"
	@echo "CFLAGS       =   ${CFLAGS}"
	@echo "CC           =   ${CC}"
	@echo

%.o: %.c
	${CC} -c ${CFLAGS} ${DEBUG_CFLAGS} $<

${PROGNAME}: ${OBJ}
	${CC} -o $@ $^ ${LDFLAGS}

clean:
	rm -f *.o ${PROGNAME}

install: all
	mkdir -p ${DESTDIR}$(PREFIX)/bin
	cp -f ${PROGNAME} ${DESTDIR}$(PREFIX)/bin
	chmod 755 ${DESTDIR}$(PREFIX)/bin/${PROGNAME}

.PHONY: all flags clean install
