/* Copyright (c) 2023 mira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

__attribute__((__always_inline__))
static inline const char *capitalize(char *);
static void dostdin(void);
static void help(const char *);

static int (*convert)(int) = &toupper;

void
dostdin(void)
{
    char c;
    char *line = NULL;
    size_t n = 0;
    ssize_t len;

    while ((len = getdelim(&line, &n, ' ', stdin)) != -1 && *line != '\n') {
        c = line[len - 1];
        if (c == ' ' || c == '\n')
            line[len - 1] = '\0';

        printf("%s ", capitalize(line));
    }
    free(line);
}

const char *
capitalize(char *str)
{
    if (str)
        *str = convert(*str);
    return str;
}

void
help(const char *progname)
{
    fprintf(stderr,
            "usage: %s [-r] [STRING...]\n"
            "  Options:\n"
            "    -h, --help    display this message\n"
            "    -r            uncapitalize STRING...\n"
            "\n"
            "  If STRING was not provided, read from stdin instead\n",
            progname);
}

int
main(int argc, char **argv)
{
    int i;

    for (i = 1; i < argc; i++) {
        if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help")) {
            help(argv[0]);
            return EXIT_SUCCESS;
        } else if (!strcmp(argv[i], "-r")) {
            convert = &tolower;
        } else if (!strcmp(argv[i], "--")) {
            i++; /* we don't want to convert -- */
            break;
        } else {
            break; /* unlike with --, we still want to convert argv[i] */
        }
    }

    if (i >= argc) {
        dostdin();
    } else {
        for (; i < argc; i++) {
            fputs(capitalize(argv[i]), stdout);
            if (i + 1 != argc)
                putchar(' ');
        }
    }

    putchar('\n');
    return 0;
}
