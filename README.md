capitalize
==========

# Introduction
capitalize is a small command line tool to (un)capitalize text.

When provided with arguments, capitalize will capitalize each one of them,
and print them back to stdout. If no arguments were provided the program will
instead read from stdin, meaning you can use it in pipelines such as:

```bash
some-program-that-prints-stuff | capitalize
```

Alternatively, if the `-r` command line is provided, capitalize do the opposite,
i.e., uncapitalize text.

**NOTE:** In such case, the `-r` argument will not get printed back to stdout.

For more information, run capitalize with the `-h` flag.


# Dependencies
capitalize has no runtime dependencies; all you need is a working C compiler and (GNU) make.


# Building

Simply run the `make` command:

```bash
make
```


# Installing

By default, running
```bash
make install
```

will install capitalize into the /usr/bin directory, however the installation path
can be changed by editing the `PREFIX` variable in the [Makefile](./Makefile),
and then running make.

You can also change the value of `PREFIX` dynamically by passing it as an argument when
running make. For instance, the following command will install capitalize to ~/.local/bin:

```bash
make PREFIX=~/.local install
```
